# Estructuras de datos de concurrencia en Go

## Colas Concurrentes (Concurrent Queues)

**Descripción:** Las colas concurrentes son estructuras de datos diseñadas para admitir operaciones seguras y eficientes en entornos concurrentes, donde múltiples goroutines (hilos ligeros) pueden acceder y modificar la cola al mismo tiempo. Estas colas están optimizadas para minimizar la contención y la sincronización entre goroutines, utilizando técnicas como bloqueos (locks), semáforos o canales para garantizar la coherencia y la seguridad de los datos.

**Casos de Uso:**

- Comunicación y sincronización entre goroutines en aplicaciones concurrentes.
- Implementación de sistemas de procesamiento de eventos o tareas distribuidas.
- Colas de trabajo en sistemas de procesamiento por lotes (batch processing) y colas de mensajes.

**Ejemplo:**

```go
package main

import (
    "fmt"
    "sync"
)

// Definición de la cola concurrente
type ConcurrentQueue struct {
    items []int
    lock  sync.Mutex
}

// Función para encolar un elemento en la cola concurrente
func (cq *ConcurrentQueue) Enqueue(item int) {
    cq.lock.Lock()
    defer cq.lock.Unlock()
    cq.items = append(cq.items, item)
}

// Función para desencolar un elemento de la cola concurrente
func (cq *ConcurrentQueue) Dequeue() (int, error) {
    cq.lock.Lock()
    defer cq.lock.Unlock()
    if len(cq.items) == 0 {
        return 0, fmt.Errorf("queue is empty")
    }
    item := cq.items[0]
    cq.items = cq.items[1:]
    return item, nil
}

func main() {
    // Crear una cola concurrente
    queue := ConcurrentQueue{}

    // Goroutine para encolar elementos
    go func() {
        for i := 0; i < 5; i++ {
            queue.Enqueue(i)
        }
    }()

    // Goroutine para desencolar elementos
    go func() {
        for i := 0; i < 5; i++ {
            item, _ := queue.Dequeue()
            fmt.Println("Dequeued:", item)
        }
    }()

    // Esperar a que las goroutines terminen
    var input string
    fmt.Scanln(&input)
}
```

Volver al índice: [Recurso Go](README.md)
