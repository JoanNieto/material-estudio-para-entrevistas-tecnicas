# Variables y constantes en Go

## Variables

En Go, una variable es un espacio de almacenamiento nombrado que se utiliza para almacenar valores que pueden cambiar durante la ejecución del programa. Debes declarar una variable antes de usarla. La declaración de una variable incluye el nombre de la variable y el tipo de datos que contendrá.
Las variables deben usarse, en caso de no ser usadas Go generará un error de sintáxis, si quiere omitir la variable (por que no será usada, por ej), puede hacerlo con _
Cuando no inicializamos una variable, esta toma el valor 0 por defecto, en caso de string ""

```go
var edad int // Declaración
edad = 30     // Asignación

nombre := "Juan" // Declaración e inicialización

var salario float64 = 2500.50 // Declaración e inicialización con tipo explícito

_ = "omitir retorno" // Omitir variable, que no será usada

```

## Constantes

Una constante es un valor que no cambia durante la ejecución del programa. En Go, se declaran utilizando la palabra clave const y deben inicializarse con un valor constante en el momento de la declaración.

```go
const nombreConstante tipoDeDato = valorConstante // Declaración con tipo de dato e inicialización

const pi = 3.1416 // Declaración e inicialización
const diasSemana = 7
const mensaje = "¡Hola, mundo!"
```

### Nota

Podemos agrupar tanto constantes como variables para facilitar la lectura y mantenibilidad del código

```go
var(
    velocidad = 0
    nombre = ""
)

const(
    gravedad = 9.8
)

func main(){

}
```

Volver al índice: [Recurso Go](README.md)