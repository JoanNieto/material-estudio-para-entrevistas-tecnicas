# Operadores en Go

## Operadores Aritméticos

- ```+```: Suma dos valores.
- ```-```: Resta el segundo valor del primero.
- ```*```: Multiplica dos valores.
- ```/```: Divide el primer valor por el segundo.
- ```%```: Devuelve el resto de la división del primer valor por el segundo.

## Operadores de Comparación

- ```==```: Comprueba si dos valores son iguales.
- ```!=```: Comprueba si dos valores son diferentes.
- ```<```: Comprueba si el primer valor es menor que el segundo.
- ```>```: Comprueba si el primer valor es mayor que el segundo.
- ```<=```: Comprueba si el primer valor es menor o igual que el segundo.
- ```>=```: Comprueba si el primer valor es mayor o igual que el segundo.

## Operadores Lógicos

- ```&&```: AND lógico. Retorna true si ambos operandos son true.
- ```||```: OR lógico. Retorna true si al menos uno de los operandos es true.
- ```!```: NOT lógico. Retorna el opuesto del valor del operando.

## Operadores de Bit

- ```&```: AND bit a bit.
- ```|```: OR bit a bit.
- ```^```: XOR bit a bit.
- ```<<```: Desplazamiento a la izquierda.
- ```>>```: Desplazamiento a la derecha.

## Ejemplo

```go
package main

import "fmt"

func main() {
    / Tipos básicos
    var entero int = 10
    var flotante float64 = 3.14
    var complejo complex128 = 2 + 3i
    var booleano bool = true
    var cadena string = "Hola, mundo!"
    var caracter byte = 'a' // Carácter Unicode
    var puntoCodigo rune = '♠' // Punto de código Unicode
    
    fmt.Println("Entero:", entero)
    fmt.Println("Flotante:", flotante)
    fmt.Println("Complejo:", complejo)
    fmt.Println("Booleano:", booleano)
    fmt.Println("Cadena:", cadena)
    fmt.Println("Carácter:", caracter)
    fmt.Println("Punto de Código:", puntoCodigo)

    // Operadores
    fmt.Println("\nOperadores:")
    resultadoSuma := entero + 5
    fmt.Println("Suma:", resultadoSuma)

    resultadoResta := flotante - 2.0
    fmt.Println("Resta:", resultadoResta)

    resultadoMultiplicacion := complejo * (2 + 1i)
    fmt.Println("Multiplicación:", resultadoMultiplicacion)

    resultadoDivision := entero / 3
    fmt.Println("División:", resultadoDivision)

    resultadoModulo := entero % 3
    fmt.Println("Módulo:", resultadoModulo)

    esIgual := entero == 10
    fmt.Println("¿Es igual a 10?", esIgual)

    esMayor := flotante > 2.0
    fmt.Println("¿Es mayor que 2.0?", esMayor)

    esMenorIgual := puntoCodigo <= 9824
    fmt.Println("¿Es menor o igual a 9824 (♠)?", esMenorIgual)

    esDiferente := cadena != "Adiós, mundo!"
    fmt.Println("¿Es diferente de 'Adiós, mundo!'?", esDiferente)

    // Operadores Lógicos
    fmt.Println("\nOperadores Lógicos:")
    resultadoLogicoAND := booleano && true
    fmt.Println("AND lógico:", resultadoLogicoAND)

    resultadoLogicoOR := booleano || false
    fmt.Println("OR lógico:", resultadoLogicoOR)

    resultadoLogicoNOT := !booleano
    fmt.Println("NOT lógico:", resultadoLogicoNOT)

    // Operadores de Bit
    fmt.Println("\nOperadores de Bit:")
    resultadoBitAND := entero & 7
    fmt.Println("AND bit a bit:", resultadoBitAND)

    resultadoBitOR := entero | 7
    fmt.Println("OR bit a bit:", resultadoBitOR)

    resultadoBitXOR := entero ^ 7
    fmt.Println("XOR bit a bit:", resultadoBitXOR)

    resultadoDesplazamientoIzq := entero << 2
    fmt.Println("Desplazamiento a la izquierda:", resultadoDesplazamientoIzq)

    resultadoDesplazamientoDer := entero >> 2
    fmt.Println("Desplazamiento a la derecha:", resultadoDesplazamientoDer)
}

```

Volver al índice: [Recurso Go](README.md)