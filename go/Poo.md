# POO en Go

La Programación Orientada a Objetos (POO) es un paradigma de programación que se basa en el concepto de "objetos", los cuales pueden contener datos en forma de campos, y código en forma de métodos. Estos objetos interactúan entre sí a través de mensajes. La POO se basa en cuatro conceptos fundamentales:

## Encapsulamiento, polimorfismo, herencia y abstracción

- **Encapsulamiento:** Es el ocultamiento de los detalles de implementación de un objeto y la exposición de una interfaz para interactuar con ese objeto. Esto promueve la modularidad y el mantenimiento del código al reducir la dependencia entre diferentes partes del programa.

En Go, la convención de nomenclatura de mayúsculas y minúsculas se utiliza para controlar la visibilidad de los campos y métodos de una estructura. Los campos o métodos que comienzan con mayúscula son públicos y accesibles desde fuera del paquete, mientras que aquellos que comienzan con minúscula son privados y solo son accesibles dentro del paquete.

- **Herencia:** Permite la creación de nuevas clases basadas en clases existentes. Las clases derivadas heredan los campos y métodos de las clases base, lo que facilita la reutilización del código y la organización jerárquica de las clases.

Go no tiene herencia de clases como en lenguajes como Java o C++. En su lugar, se fomenta la composición, donde las estructuras pueden contener otras estructuras como campos. Esto promueve un diseño más flexible y favorece la composición sobre la herencia.

- **Polimorfismo:** Es la capacidad de objetos de diferentes tipos de responder al mismo mensaje de manera diferente. Esto permite tratar objetos de diferentes tipos de manera uniforme a través de una interfaz común, lo que facilita la flexibilidad y la extensibilidad del código.

En Go, el polimorfismo se logra a través de interfaces. Una interfaz es un conjunto de métodos. Cualquier tipo que implemente todos los métodos de una interfaz se considera implícitamente como implementación de esa interfaz. Esto permite que varios tipos diferentes se traten de manera uniforme a través de una interfaz común.

- **Abstracción:** Consiste en la simplificación de la representación de un objeto enfocándose en los aspectos esenciales y ocultando los detalles innecesarios. La abstracción se logra mediante la definición de clases y métodos que representan conceptos del mundo real de manera abstracta.

Go proporciona mecanismos para crear abstracciones de datos y comportamientos utilizando estructuras, métodos y interfaces. Esto permite modelar conceptos del mundo real de manera abstracta y promueve la reutilización del código.

Ejemplo:

```go
package main

import "fmt"

// Definición de una interfaz para formas geométricas
type Shape interface {
    Area() float64
}

// Definición de una estructura de círculo
type Circle struct {
    Radius float64
}

// Implementación del método de área para el círculo
func (c Circle) Area() float64 {
    return 3.14 * c.Radius * c.Radius
}

// Definición de una estructura de rectángulo
type Rectangle struct {
    Width  float64
    Height float64
}

// Implementación del método de área para el rectángulo
func (r Rectangle) Area() float64 {
    return r.Width * r.Height
}

func main() {
    // Crear un círculo y un rectángulo
    circle := Circle{Radius: 5}
    rectangle := Rectangle{Width: 4, Height: 3}

    // Calcular área usando polimorfismo
    fmt.Println("Área del círculo:", getArea(circle))
    fmt.Println("Área del rectángulo:", getArea(rectangle))
}

// Función que calcula el área de cualquier forma geométrica
func getArea(shape Shape) float64 {
    return shape.Area()
}
```

## Bajo acoplamiento y fuerte cohesión

- **Acoplamiento**: El acoplamiento es el grado en que los módulos de un programa dependen unos de otros. Si para hacer cambios en un módulo del programa es necesario hacer cambios en otro módulo distinto, existe acoplamiento entre ambos módulos.

- **Cohesion**: Se refiere a la medida en que los componentes de un módulo están relacionados entre sí y trabajan juntos para lograr un propósito específico. Un módulo con alta cohesión tiene funcionalidades relacionadas agrupadas juntas de manera lógica y consistente.

### ¿Que es lo ideal?

![Cohesión y acoplamiento](../img/cohesion-acoplamiento.webp "Cohesión y acoplamiento")

Lo que buscamos es alta cohesión y bajo acoplamiento.

**Ejemplo:**

Supongamos que estamos diseñando un sistema de procesamiento de pagos. Podemos tener varias clases que manejan diferentes aspectos del sistema, como ProcesadorDePagos, ValidadorDeTarjetas, NotificadorDePago y BaseDeDatosDePagos. Cada una de estas clases tiene una responsabilidad única y bien definida. La alta cohesión se logra al agrupar funcionalidades relacionadas en cada clase. El bajo acoplamiento se logra al minimizar las dependencias entre estas clases, lo que permite modificar o reemplazar una clase sin afectar significativamente a otras.

```go
package main

import "fmt"

// ProcesadorDePagos maneja el procesamiento de pagos
type ProcesadorDePagos struct{}

// Procesar realiza el procesamiento de un pago
func (p *ProcesadorDePagos) Procesar(monto float64) error {
    fmt.Printf("Procesando pago de %.2f\n", monto)
    return nil
}

// ValidadorDeTarjetas maneja la validación de tarjetas de crédito
type ValidadorDeTarjetas struct{}

// Validar verifica la validez de una tarjeta de crédito
func (v *ValidadorDeTarjetas) Validar(numero string) bool {
    fmt.Println("Validando tarjeta:", numero)
    // Lógica de validación simplificada
    return len(numero) == 16
}

// NotificadorDePago maneja la notificación de pagos
type NotificadorDePago struct{}

// Notificar envía una notificación de pago
func (n *NotificadorDePago) Notificar(correo string) error {
    fmt.Println("Enviando notificación a:", correo)
    return nil
}

// BaseDeDatosDePagos maneja el almacenamiento de pagos
type BaseDeDatosDePagos struct{}

// Guardar almacena un registro de pago en la base de datos
func (db *BaseDeDatosDePagos) Guardar(monto float64) error {
    fmt.Printf("Guardando pago de %.2f en la base de datos\n", monto)
    return nil
}

func main() {
    procesador := ProcesadorDePagos{}
    validador := ValidadorDeTarjetas{}
    notificador := NotificadorDePago{}
    db := BaseDeDatosDePagos{}

    // Lógica de procesamiento de pagos
    monto := 100.50
    if err := procesador.Procesar(monto); err != nil {
        fmt.Println("Error al procesar el pago:", err)
        return
    }

    numeroTarjeta := "1234567890123456"
    if !validador.Validar(numeroTarjeta) {
        fmt.Println("Tarjeta inválida")
        return
    }

    if err := notificador.Notificar("usuario@example.com"); err != nil {
        fmt.Println("Error al enviar la notificación:", err)
        return
    }

    if err := db.Guardar(monto); err != nil {
        fmt.Println("Error al guardar el registro de pago:", err)
        return
    }

    fmt.Println("Procesamiento de pago completado")
}

```

## Interfaces y Clases Abstractas

- **Interfaces**: En Go, una interfaz es un conjunto de métodos que define un comportamiento. Una interfaz describe qué métodos deben ser implementados por un tipo para satisfacer esa interfaz. Los tipos pueden implementar una interfaz si implementan todos los métodos especificados en la interfaz. Las interfaces promueven el polimorfismo y la abstracción al permitir que diferentes tipos compartan un comportamiento común.

```go
package main

import (
    "fmt"
    "math"
)

// Definimos una interfaz llamada Forma que tiene un método Area()
type Forma interface {
    Area() float64
}

// Definimos una estructura llamada Rectangulo que implementa la interfaz Forma
type Rectangulo struct {
    Base   float64
    Altura float64
}

// Implementación del método Area para Rectangulo
func (r Rectangulo) Area() float64 {
    return r.Base * r.Altura
}

// Definimos una estructura llamada Circulo que implementa la interfaz Forma
type Circulo struct {
    Radio float64
}

// Implementación del método Area para Circulo
func (c Circulo) Area() float64 {
    return math.Pi * c.Radio * c.Radio
}

// Función que toma cualquier tipo que implemente la interfaz Forma
func ImprimirArea(f Forma) {
    fmt.Printf("El área es: %.2f\n", f.Area())
}

func main() {
    // Creamos una instancia de Rectangulo
    rectangulo := Rectangulo{Base: 5, Altura: 3}
    // Imprimimos el área del rectángulo
    ImprimirArea(rectangulo)

    // Creamos una instancia de Circulo
    circulo := Circulo{Radio: 2}
    // Imprimimos el área del círculo
    ImprimirArea(circulo)
}
```

- **Clases Abstractas**: A diferencia de algunos lenguajes orientados a objetos, como Java o C++, Go no tiene clases abstractas en el sentido tradicional. Sin embargo, se pueden emular utilizando interfaces. Una clase abstracta es una clase que no puede ser instanciada directamente, sino que proporciona una estructura común y métodos abstractos que deben ser implementados por sus subclases. En Go, podemos definir una interfaz con métodos "abstractos" y permitir que los tipos la implementen según sea necesario.

## Inner classes y clases anónimas

- **Inner class**: El concepto de inner class se refiere a una clase que está definida dentro de otra clase. En Go, esto se puede lograr definiendo un tipo dentro del cuerpo de otro tipo, similar a cómo se definen las estructuras anidadas. Sin embargo, en Go, los tipos anidados no tienen acceso privilegiado a los campos del tipo externo.

```go
package main

import "fmt"

// Definimos un tipo externo
package main

import "fmt"

// Definimos un tipo externo
type Exterior struct {
    Externo int

    // Definimos un tipo interno
    Interno struct {
        Interno int
    }
}

func main() {
    exterior := Exterior{
        Externo: 10,
        Interno: struct{ Interno int }{Interno: 20},
    }

    fmt.Println("Externo:", exterior.Externo)
    fmt.Println("Interno:", exterior.Interno.Interno)
}
```

- **Clase anónima**: En algunos lenguajes de programación, como Java, se pueden definir clases anónimas como una forma de implementar interfaces o clases abstractas sin tener que definir una clase separada. En Go, no hay constructores de clases anónimas en el mismo sentido, pero se pueden lograr utilizando tipos anónimos y literales de estructuras.

```go
package main

import "fmt"

// Definimos una interfaz
type Saludador interface {
    Saludar() string
}

func main() {
    // Creamos una instancia de la interfaz Saludador utilizando un tipo anónimo
    saludador := Saludador(func() string {
        return "¡Hola mundo desde una clase anónima!"
    })

    // Llamamos al método Saludar() en la instancia de la interfaz
    fmt.Println(saludador.Saludar())
}
```

Volver al índice: [Recurso Go](README.md)