# DRY (Keep It Simple, Stupid)

Mantenlo Simple, Estúpido. Es un principio de diseño que aboga por mantener las soluciones simples y fáciles de entender en lugar de crear soluciones complejas que pueden ser difíciles de entender y mantener.

Ejemplo:

Suponga el caso anterior de creación de tarjetas, donde debamos validar que el tipo solo sea "débito" o "crédito"

**Mala implementación**:

```go
package main

import "fmt"

type Usuario struct {
  ID   int
  Name string
}

type CuentaBancaria struct {
  ID     int
  Numero int
}

type Tarjeta struct {
  NumeroTarjeta int
  Tipo          string
  Usuario       *Usuario
  Cuenta        *CuentaBancaria
}

func main() {
  usuario := &Usuario{
    ID:   1,
    Name: "Joan",
  }
  cuenta := &CuentaBancaria{
    ID:     1,
    Numero: 1234567890,
  }
  tarjetaDebito, err := CrearTarjeta(1234567890, "Debito", usuario, cuenta)
  if err != nil {
    fmt.Println(err)
    return
  }

  fmt.Printf("Tarjeta: %v, Tipo: %v, Usuario: %v, Cuenta: %v\n", tarjetaDebito.NumeroTarjeta, tarjetaDebito.Tipo, tarjetaDebito.Usuario, tarjetaDebito.Cuenta)
}

func CrearTarjeta(numeroTarjeta int, tipo string, usuario *Usuario, cuenta *CuentaBancaria) (*Tarjeta, error) {
  if tipo == "Debito"{
    return &Tarjeta{
      NumeroTarjeta: numeroTarjeta,
      Tipo:          "debito",
      Usuario:       usuario,
      Cuenta:        cuenta,
    }, nil
  }else if tipo == "Credito"{
    return &Tarjeta{
      NumeroTarjeta: numeroTarjeta,
      Tipo:          "credito",
      Usuario:       usuario,
      Cuenta:        cuenta,
    },nil
  } else {
    return nil, fmt.Errorf("tipo de tarjeta no valido")
  }
}

```

Como se puede observar nuestro código tiene lógica innecesaria y se vuelve complicado

**Refactorizando usando KISS**:

```go
package main

import "fmt"

type Usuario struct {
  ID   int
  Name string
}

type CuentaBancaria struct {
  ID     int
  Numero int
}

type Tarjeta struct {
  NumeroTarjeta int
  Tipo          string
  Usuario       *Usuario
  Cuenta        *CuentaBancaria
}

func main() {
  usuario := &Usuario{
    ID:   1,
    Name: "Joan",
  }
  cuenta := &CuentaBancaria{
    ID:     1,
    Numero: 1234567890,
  }
  tarjetaDebito, err := CrearTarjeta(1234567890, "Debito", usuario, cuenta)
  if err != nil {
    fmt.Println(err)
    return
  }

  fmt.Printf("Tarjeta: %v, Tipo: %v, Usuario: %v, Cuenta: %v\n", tarjetaDebito.NumeroTarjeta, tarjetaDebito.Tipo, tarjetaDebito.Usuario, tarjetaDebito.Cuenta)
}

func CrearTarjeta(numeroTarjeta int, tipo string, usuario *Usuario, cuenta *CuentaBancaria) (*Tarjeta, error) {
  if tipo != "Debito" || tipo != "Credito" {
    return nil, fmt.Errorf("tipo de tarjeta no valido")
  }

  return &Tarjeta{
    NumeroTarjeta: numeroTarjeta,
    Tipo:          tipo,
    Usuario:       usuario,
    Cuenta:        cuenta,
  }, nil
}

```

Volver al índice: [Recurso Go](README.md)