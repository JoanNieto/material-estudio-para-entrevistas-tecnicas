# Análisis de complejidad Big-O Notation

## ¿Qué es?

La notación Big-O es una herramienta utilizada en ciencias de la computación para describir el comportamiento del tiempo de ejecución o el uso de recursos (como la memoria) de un algoritmo en relación con el tamaño de la entrada. Proporciona una forma de cuantificar el rendimiento relativo de un algoritmo a medida que el tamaño de los datos de entrada aumenta.

## ¿Para qué sirve?

La notación Big-O sirve para analizar y comparar la eficiencia de los algoritmos en términos de su tiempo de ejecución y uso de recursos. Permite a los desarrolladores entender cómo crecen los recursos requeridos por un algoritmo a medida que el tamaño del problema aumenta, lo que es crucial para diseñar sistemas eficientes y escalables.

![Big O Notation](../img/big_o_notation.webp "Title")
*Gráfica de complejidad Big O*

## Tipos de complejidad

1. O(1) - Constant Time:

   Significa que el tiempo de ejecución del algoritmo es constante, independientemente del tamaño de la entrada. Es el escenario ideal y representa la eficiencia máxima.

   Ejemplo: Acceder a un elemento específico en un arreglo por su índice.

2. O(log n) - Logarithmic Time:

   El tiempo de ejecución del algoritmo crece de forma logarítmica en relación con el tamaño de la entrada. Este tipo de complejidad suele ser muy eficiente.

   Ejemplo: Búsqueda binaria en un arreglo ordenado.

3. O(n) - Linear Time:

    El tiempo de ejecución del algoritmo crece linealmente en relación con el tamaño de la entrada.

    Ejemplo: Recorrer una lista o un arreglo una vez.

4. O(n log n) - Log-Linear Time:

    El tiempo de ejecución del algoritmo crece en proporción a n veces el logaritmo de n. Es común en algoritmos de ordenación eficientes.

    Ejemplo: Algoritmos de ordenación como Merge Sort y Quick Sort.

5. O(n^2) - Quadratic Time:

    El tiempo de ejecución del algoritmo crece cuadráticamente en relación con el tamaño de la entrada. Este tipo de complejidad puede volverse prohibitivo para tamaños grandes de entrada.

    Ejemplo: Bucles anidados que recorren una matriz bidimensional.

6. O(2^n) - Exponential Time:

    El tiempo de ejecución del algoritmo crece exponencialmente en relación con el tamaño de la entrada. Es inherentemente ineficiente y puede resultar impracticable para entradas moderadamente grandes.

    Ejemplo: Algoritmos de fuerza bruta para problemas de combinaciones o subconjuntos.

7. O(n!) - Factorial Time:

    El tiempo de ejecución del algoritmo crece factorialmente en relación con el tamaño de la entrada. Es la peor complejidad posible y generalmente no es viable para problemas prácticos.

    Ejemplo: Algoritmos de permutación total como el problema del viajante de comercio (TSP).

Articulos de interés:

- [Introducción a Big O Notation - Medium](https://medium.com/nowports-tech/introducci%C3%B3n-a-big-o-notation-95ecca8bd866)
- [Confused by Big O Notation? A Newbie’s Guide to Understand it Once and For All - Medium](https://medium.com/@yuribett/confused-by-big-o-notation-a-newbies-guide-to-understand-it-once-and-for-all-23aff8b84d60)

Volver al índice: [Recurso Go](README.md)