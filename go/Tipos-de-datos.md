# Tipos de Datos en Go

## Tipos Básicos

Los tipos básicos en Go son los bloques de construcción fundamentales para la declaración de variables y la definición de funciones. Estos tipos incluyen:

### Números Enteros

- `int`: Entero con signo. Su tamaño depende de la arquitectura.
- `int8`, `int16`, `int32`, `int64`: Enteros con signo de 8, 16, 32 y 64 bits respectivamente.
- `uint`: Entero sin signo. Su tamaño depende de la arquitectura.
- `uint8`, `uint16`, `uint32`, `uint64`: Enteros sin signo de 8, 16, 32 y 64 bits respectivamente.
- `uintptr`: Entero sin signo lo suficientemente grande como para contener el valor de un puntero.

### Números de Punto Flotante

- `float32`, `float64`: Números de punto flotante de 32 y 64 bits respectivamente.

### Números Complejos

- `complex64`, `complex128`: Números complejos de 64 y 128 bits respectivamente.

### Booleanos

- `bool`: Tipo de datos booleano que puede ser `true` o `false`.

### Cadenas de Caracteres

- `string`: Secuencia de bytes de solo lectura.

### Caracteres

- `byte`: Alias para `uint8`, representa un byte de datos.
- `rune`: Alias para `int32`, representa un punto de código Unicode.

## Tipos Compuestos

Los tipos compuestos en Go son aquellos que están formados por la combinación de uno o más tipos básicos. Los tipos compuestos incluyen:

### Arrays

- `array`: Colección fija de elementos del mismo tipo.

### Slices

- `slice`: Segmento variable de un array.

### Mapas

- `map`: Colección no ordenada de pares clave-valor.

### Estructuras

- `struct`: Colección de campos con nombre.

## Tipos Definidos por el Usuario

En Go, los desarrolladores también pueden definir sus propios tipos de datos mediante la declaración de nuevos tipos utilizando la palabra clave `type`. Por ejemplo:

```go
type MiTipo int
```

## Ejemplo

```go
package main

import "fmt"

func main() {
    // Tipos básicos
    var entero int = 10
    var flotante float64 = 3.14
    var complejo complex128 = 2 + 3i
    var booleano bool = true
    var cadena string = "Hola, mundo!"
    var caracter byte = 'a' // Carácter Unicode
    var puntoCodigo rune = '♠' // Punto de código Unicode
    
    fmt.Println("Entero:", entero)
    fmt.Println("Flotante:", flotante)
    fmt.Println("Complejo:", complejo)
    fmt.Println("Booleano:", booleano)
    fmt.Println("Cadena:", cadena)
    fmt.Println("Carácter:", caracter)
    fmt.Println("Punto de Código:", puntoCodigo)

    // Tipos compuestos
    // Arrays
    var arrayEnteros [3]int = [3]int{1, 2, 3}
    fmt.Println("Array de Enteros:", arrayEnteros)

    // Slices
    sliceEnteros := []int{4, 5, 6}
    fmt.Println("Slice de Enteros:", sliceEnteros)

    // Mapas
    mapaFrutas := map[string]string{
        "manzana": "roja",
        "plátano": "amarillo",
        "uva":     "morada",
    }
    fmt.Println("Mapa de Frutas:", mapaFrutas)

    // Estructuras
    type Persona struct {
        Nombre string
        Edad   int
    }
    persona := Persona{"Juan", 30}
    fmt.Println("Estructura Persona:", persona)
}

```

Volver al índice: [Recurso Go](README.md)