# Estructuras de datos de grafos en Go

## Grafos (Graphs)

**Descripción:** Un grafo es una estructura de datos que consiste en un conjunto de vértices (nodos) y un conjunto de aristas que conectan algunos de estos vértices. Los grafos se utilizan para representar relaciones entre objetos en una variedad de aplicaciones, como redes sociales, sistemas de transporte y análisis de rutas.

**Casos de Uso:**

- Modelado de relaciones entre entidades en una variedad de dominios, como redes de computadoras, redes sociales, etc.
- Algoritmos de búsqueda de rutas, como el algoritmo de Dijkstra o el algoritmo de búsqueda en profundidad (DFS).
- Resolución de problemas de flujo de información, como el algoritmo de flujo máximo.

**Ejemplo:**

```go
package main

import (
    "fmt"
)

// Definición de un grafo como una lista de adyacencia
type Graph struct {
    vertices map[int][]int
}

// Función para añadir una arista al grafo
func (g *Graph) addEdge(u, v int) {
    g.vertices[u] = append(g.vertices[u], v)
    // Para un grafo no dirigido, descomentar la siguiente línea
    // g.vertices[v] = append(g.vertices[v], u)
}

// Función para imprimir el grafo
func (g *Graph) printGraph() {
    for vertex, edges := range g.vertices {
        fmt.Printf("Vertex %d -> ", vertex)
        for _, edge := range edges {
            fmt.Printf("%d ", edge)
        }
        fmt.Println()
    }
}

func main() {
    // Creación de un grafo
    g := Graph{vertices: make(map[int][]int)}
    
    // Añadir aristas
    g.addEdge(0, 1)
    g.addEdge(0, 2)
    g.addEdge(1, 2)
    g.addEdge(1, 3)
    g.addEdge(2, 3)
    
    // Imprimir el grafo
    g.printGraph()
}
```

Volver al índice: [Recurso Go](README.md)
