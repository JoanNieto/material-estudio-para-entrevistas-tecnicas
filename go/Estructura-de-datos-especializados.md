# Estructuras de datos especializados en Go

## Tries (Árbol de Prefijos)

**Descripción:** Un Trie, también conocido como árbol de prefijos, es una estructura de datos especializada utilizada para almacenar un conjunto de cadenas de caracteres donde las claves compartidas se almacenan de manera eficiente utilizando estructuras de árbol. Cada nodo del trie representa un carácter y las aristas salientes representan las transiciones a los caracteres siguientes.

**Casos de Uso:**

- Almacenamiento y recuperación eficientes de palabras completas y sus prefijos comunes.
- Búsqueda rápida de palabras en diccionarios y sistemas de autocompletado.
- Implementación de algoritmos de búsqueda de palabras y verificación de ortografía.

**Ejemplo:**

```go
package main

import "fmt"

// Definición de un nodo del Trie
type TrieNode struct {
    children map[rune]*TrieNode
    isEnd    bool
}

// Función para inicializar un nuevo nodo Trie
func newTrieNode() *TrieNode {
    return &TrieNode{
        children: make(map[rune]*TrieNode),
        isEnd:    false,
    }
}

// Definición de la estructura Trie
type Trie struct {
    root *TrieNode
}

// Función para inicializar un nuevo Trie
func newTrie() *Trie {
    return &Trie{
        root: newTrieNode(),
    }
}

// Función para insertar una palabra en el Trie
func (t *Trie) insert(word string) {
    node := t.root
    for _, char := range word {
        if _, ok := node.children[char]; !ok {
            node.children[char] = newTrieNode()
        }
        node = node.children[char]
    }
    node.isEnd = true
}

// Función para buscar una palabra en el Trie
func (t *Trie) search(word string) bool {
    node := t.root
    for _, char := range word {
        if _, ok := node.children[char]; !ok {
            return false
        }
        node = node.children[char]
    }
    return node.isEnd
}

func main() {
    trie := newTrie()
    words := []string{"apple", "banana", "orange", "pear", "peach"}

    // Insertar palabras en el Trie
    for _, word := range words {
        trie.insert(word)
    }

    // Buscar palabras en el Trie
    fmt.Println("Search 'apple':", trie.search("apple"))   // true
    fmt.Println("Search 'banana':", trie.search("banana")) // true
    fmt.Println("Search 'grape':", trie.search("grape"))   // false
}
```

## Matrices Dispersas (Sparse Matrices)

**Descripción:** Una matriz dispersa es una estructura de datos que se utiliza para representar matrices donde la mayoría de los elementos tienen un valor cero. En lugar de almacenar todos los elementos de la matriz, solo se almacenan los elementos distintos de cero junto con sus índices. Esto ahorra espacio de almacenamiento y mejora la eficiencia en operaciones como la multiplicación de matrices.

**Casos de Uso:**

- Almacenamiento eficiente de matrices grandes con muchos elementos cero.
- Reducción de la sobrecarga de almacenamiento y optimización de operaciones matriciales.
- Aplicaciones en procesamiento de imágenes, cálculo científico y sistemas de recomendación.

**Ejemplo:**

```go
package main

import "fmt"

// Definición de la estructura SparseMatrix
type SparseMatrix map[int]map[int]int

// Función para inicializar una nueva matriz dispersa
func newSparseMatrix() SparseMatrix {
    return make(SparseMatrix)
}

// Función para establecer un valor en la matriz dispersa
func (sm SparseMatrix) set(row, col, val int) {
    if sm[row] == nil {
        sm[row] = make(map[int]int)
    }
    sm[row][col] = val
}

// Función para obtener un valor de la matriz dispersa
func (sm SparseMatrix) get(row, col int) int {
    if sm[row] == nil || sm[row][col] == 0 {
        return 0
    }
    return sm[row][col]
}

func main() {
    // Crear una nueva matriz dispersa
    matrix := newSparseMatrix()

    // Establecer valores en la matriz dispersa
    matrix.set(0, 1, 2)
    matrix.set(1, 0, 3)
    matrix.set(2, 2, 4)

    // Obtener valores de la matriz dispersa
    fmt.Println("Value at (0, 1):", matrix.get(0, 1)) // 2
    fmt.Println("Value at (1, 0):", matrix.get(1, 0)) // 3
    fmt.Println("Value at (2, 1):", matrix.get(2, 1)) // 0
}
```

## Listas Doblemente Terminadas (Doubly Ended Lists)

**Descripción:** Una lista doblemente terminada, también conocida como lista doblemente enlazada, es una estructura de datos lineal donde cada nodo contiene un elemento y referencias a los nodos anterior y siguiente en la lista. Esto permite un acceso rápido tanto al principio como al final de la lista, así como la inserción y eliminación eficientes en cualquier posición.

**Casos de Uso:**

- Implementación de estructuras de datos como colas y listas circulares.
- Operaciones que requieren acceso rápido al inicio y al final de la lista.
- Algoritmos que necesitan inserciones y eliminaciones eficientes en posiciones arbitrarias de la lista.

**Ejemplo:**

```go
package main

import "fmt"

// Definición del nodo de la lista doblemente enlazada
type Node struct {
    data     int
    previous *Node
    next     *Node
}

// Definición de la lista doblemente enlazada
type DoublyLinkedList struct {
    head *Node
    tail *Node
}

// Función para insertar un elemento al inicio de la lista
func (dll *DoublyLinkedList) insertAtBeginning(data int) {
    newNode := &Node{data: data}
    if dll.head == nil {
        dll.head = newNode
        dll.tail = newNode
        return
    }
    newNode.next = dll.head
    dll.head.previous = newNode
    dll.head = newNode
}

// Función para insertar un elemento al final de la lista
func (dll *DoublyLinkedList) insertAtEnd(data int) {
    newNode := &Node{data: data}
    if dll.head == nil {
        dll.head = newNode
        dll.tail = newNode
        return
    }
    dll.tail.next = newNode
    newNode.previous = dll.tail
    dll.tail = newNode
}

// Función para imprimir la lista doblemente enlazada hacia adelante
func (dll *DoublyLinkedList) printForward() {
    current := dll.head
    for current != nil {
        fmt.Printf("%d ", current.data)
        current = current.next
    }
    fmt.Println()
}

// Función para imprimir la lista doblemente enlazada hacia atrás
func (dll *DoublyLinkedList) printBackward() {
    current := dll.tail
    for current != nil {
        fmt.Printf("%d ", current.data)
        current = current.previous
    }
    fmt.Println()
}

func main() {
    // Creación de una lista doblemente enlazada
    dll := DoublyLinkedList{}

    // Insertar elementos al principio de la lista
    dll.insertAtBeginning(10)
    dll.insertAtBeginning(20)
    dll.insertAtBeginning(30)

    // Insertar elementos al final de la lista
    dll.insertAtEnd(40)
    dll.insertAtEnd(50)
    dll.insertAtEnd(60)

    // Imprimir la lista hacia adelante
    fmt.Println("Doubly Linked List (Forward):")
    dll.printForward() // Output: 30 20 10 40 50 60

    // Imprimir la lista hacia atrás
    fmt.Println("Doubly Linked List (Backward):")
    dll.printBackward() // Output: 60 50 40 10 20 30
}
```

Volver al índice: [Recurso Go](README.md)
