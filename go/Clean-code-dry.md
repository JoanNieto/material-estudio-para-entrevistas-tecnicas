# DRY (Don't Repeat Yourself)

DRY, o "Don't Repeat Yourself" (No te repitas), es un principio de diseño de software que promueve la reducción de la duplicación de código. La idea principal detrás de DRY es que cada pieza de conocimiento en un sistema debe tener una representación única y no ambigua dentro de ese sistema. En otras palabras, si tienes una pieza de información en tu código que se repite en múltiples lugares, deberías refactorizar el código para evitar la duplicación.

El principio DRY sugiere que los desarrolladores deben buscar oportunidades para refactorizar el código y extraer partes comunes en funciones, clases o módulos reutilizables. Esto no solo reduce la duplicación de código, sino que también hace que el código sea más fácil de entender, mantener y modificar en el futuro.

Ejemplo:

Suponga que va a emitir tarjetas (débito, crédito y de recompensas), para ello necesita la información del usuario y la cuenta bancaria, así como datos propios de la tarjeta como el tipo y el número de tarjeta.

**Mala implementación**:

```go
package main

import "fmt"

type Usuario struct {
  ID   int
  Name string
}

type CuentaBancaria struct {
  ID     int
  Numero int
}

type Tarjeta struct {
  NumeroTarjeta int
  Tipo          string
  Usuario       *Usuario
  Cuenta        *CuentaBancaria
}

func main() {
  usuario := &Usuario{
    ID:   1,
    Name: "Joan",
  }
  cuenta := &CuentaBancaria{
    ID:     1,
    Numero: 1234567890,
  }
  tarjetaDebito := CrearTarjetaDebito(1234567890, usuario, cuenta)

  fmt.Printf("Tarjeta: %v, Tipo: %v, Usuario: %v, Cuenta: %v\n", tarjetaDebito.NumeroTarjeta, tarjetaDebito.Tipo, tarjetaDebito.Usuario, tarjetaDebito.Cuenta)
}

func CrearTarjetaDebito(numeroTarjeta int, usuario *Usuario, cuenta *CuentaBancaria) *Tarjeta {
  return &Tarjeta{
    NumeroTarjeta: numeroTarjeta,
    Tipo:          "debito",
    Usuario:       usuario,
    Cuenta:        cuenta,
  }
}

func CrearTarjetaCredito(numeroTarjeta int, usuario *Usuario, cuenta *CuentaBancaria) *Tarjeta {
  return &Tarjeta{
    NumeroTarjeta: numeroTarjeta,
    Tipo:          "credito",
    Usuario:       usuario,
    Cuenta:        cuenta,
  }
}

func CrearTarjetaRecompensas(numeroTarjeta int, usuario *Usuario, cuenta *CuentaBancaria) *Tarjeta {
  return &Tarjeta{
    NumeroTarjeta: numeroTarjeta,
    Tipo:          "recompensas",
    Usuario:       usuario,
    Cuenta:        cuenta,
  }
}

```

Como se puede observar tenemos código repetido que no necesariamente usaremos

**Refactorizando usando DRY**:

```go
package main

import "fmt"

type Usuario struct {
  ID   int
  Name string
}

type CuentaBancaria struct {
  ID     int
  Numero int
}

type Tarjeta struct {
  NumeroTarjeta int
  Tipo          string
  Usuario       *Usuario
  Cuenta        *CuentaBancaria
}

func main() {
  usuario := &Usuario{
    ID:   1,
    Name: "Joan",
  }
  cuenta := &CuentaBancaria{
    ID:     1,
    Numero: 1234567890,
  }
  tarjetaDebito := CrearTarjeta(1234567890, "Debito", usuario, cuenta)

  fmt.Printf("Tarjeta: %v, Tipo: %v, Usuario: %v, Cuenta: %v\n", tarjetaDebito.NumeroTarjeta, tarjetaDebito.Tipo, tarjetaDebito.Usuario, tarjetaDebito.Cuenta)
}

func CrearTarjeta(numeroTarjeta int, tipo string, usuario *Usuario, cuenta *CuentaBancaria) *Tarjeta {
  return &Tarjeta{
    NumeroTarjeta: numeroTarjeta,
    Tipo:          tipo,
    Usuario:       usuario,
    Cuenta:        cuenta,
  }
}

```

Volver al índice: [Recurso Go](README.md)