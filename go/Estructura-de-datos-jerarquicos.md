# Estructuras de datos jerarquicos en Go

## Arboles (Trees)

**Descripción:** Un árbol es una estructura de datos jerárquica que consiste en nodos, con un nodo raíz y subnodos hijos. Cada nodo contiene un valor y referencias a sus nodos hijos. Los árboles son una generalización de las listas enlazadas, con la diferencia de que cada nodo puede tener más de un hijo.

**Casos de Uso:**

- Almacenamiento jerárquico de datos, como sistemas de archivos.
- Implementación de bases de datos y motores de búsqueda.
- Representación de estructuras de organización.

**Ejemplo:**

```go
package main

import "fmt"

// Definición de un nodo del árbol
type TreeNode struct {
    value int
    left  *TreeNode
    right *TreeNode
}

// Función para insertar un nuevo valor en el árbol
func insert(root *TreeNode, value int) *TreeNode {
    if root == nil {
        return &TreeNode{value: value}
    }
    if value < root.value {
        root.left = insert(root.left, value)
    } else {
        root.right = insert(root.right, value)
    }
    return root
}

// Función para realizar un recorrido en orden (in-order traversal)
func inOrderTraversal(root *TreeNode) {
    if root != nil {
        inOrderTraversal(root.left)
        fmt.Printf("%d ", root.value)
        inOrderTraversal(root.right)
    }
}

func main() {
    root := &TreeNode{value: 10}
    root = insert(root, 5)
    root = insert(root, 15)
    root = insert(root, 3)
    root = insert(root, 7)
    root = insert(root, 13)
    root = insert(root, 18)
    
    fmt.Println("In-order traversal:")
    inOrderTraversal(root) // Output: 3 5 7 10 13 15 18
}
```

## Montículos (Heaps)

**Descripción:** Un montículo (heap) es una estructura de datos basada en árboles binarios que satisface la propiedad del montículo: en un montículo máximo, para cualquier nodo i, el valor de i es mayor o igual que los valores de sus hijos; en un montículo mínimo, el valor de i es menor o igual que los valores de sus hijos. Los montículos se utilizan a menudo para implementar colas de prioridad.

**Casos de Uso:**

- Implementación de colas de prioridad.
- Algoritmos de selección y ordenación, como el ordenamiento por montículo (heap sort).
- Encontrar el elemento mínimo o máximo en colecciones dinámicas.

**Ejemplo:**

```go
package main

import (
    "container/heap"
    "fmt"
)

// Un Montículo Min (Priority Queue) implementado con heap.Interface
type MinHeap []int

func (h MinHeap) Len() int           { return len(h) }
func (h MinHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h MinHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *MinHeap) Push(x interface{}) {
    *h = append(*h, x.(int))
}

func (h *MinHeap) Pop() interface{} {
    old := *h
    n := len(old)
    x := old[n-1]
    *h = old[0 : n-1]
    return x
}

func main() {
    h := &MinHeap{10, 20, 15, 30, 40}
    heap.Init(h)
    
    heap.Push(h, 5)
    fmt.Printf("Min heap: %v\n", *h) // Output: Min heap: [5 20 10 30 40 15]

    min := heap.Pop(h).(int)
    fmt.Printf("Min element: %d\n", min) // Output: Min element: 5
    fmt.Printf("Min heap after pop: %v\n", *h) // Output: Min heap after pop: [10 20 15 30 40]
}
```

Volver al índice: [Recurso Go](README.md)
