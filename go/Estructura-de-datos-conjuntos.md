# Estructuras de datos de conuntos en Go

## Conjuntos (Sets)

**Descripción:** Un conjunto es una colección desordenada de elementos únicos. En un conjunto, no hay elementos duplicados y el orden de los elementos no se garantiza. Los conjuntos se utilizan para modelar colecciones donde la presencia de elementos únicos es más relevante que su orden.

**Casos de Uso:**

- Eliminación de duplicados de una lista de elementos.
- Comprobación de la pertenencia de un elemento a un conjunto.
- Operaciones de unión, intersección y diferencia entre conjuntos.

**Ejemplo:**

```go
package main

import (
    "fmt"
)

// Definición de un conjunto utilizando un mapa
type Set map[int]bool

// Función para añadir un elemento al conjunto
func (s Set) Add(elem int) {
    s[elem] = true
}

// Función para eliminar un elemento del conjunto
func (s Set) Remove(elem int) {
    delete(s, elem)
}

// Función para comprobar si un elemento está presente en el conjunto
func (s Set) Contains(elem int) bool {
    return s[elem]
}

// Función para obtener la cardinalidad del conjunto
func (s Set) Size() int {
    return len(s)
}

func main() {
    // Creación de un conjunto
    set := make(Set)
    
    // Añadir elementos al conjunto
    set.Add(10)
    set.Add(20)
    set.Add(30)
    
    // Imprimir el conjunto y su tamaño
    fmt.Println("Set:", set)
    fmt.Println("Size:", set.Size())
    
    // Comprobar la presencia de un elemento en el conjunto
    fmt.Println("Contains 20:", set.Contains(20))
    
    // Eliminar un elemento del conjunto
    set.Remove(20)
    fmt.Println("Set after removing 20:", set)
}
```

## Tablas Hash (Hash Tables)

**Descripción:** Una tabla hash es una estructura de datos que implementa una asociación de claves-valor, donde cada clave se asigna a un único valor mediante una función de dispersión (hash). Las tablas hash son eficientes para la búsqueda, inserción y eliminación de elementos, con un tiempo de acceso promedio O(1).

**Casos de Uso:**

- Implementación de diccionarios y conjuntos.
- Almacenamiento y recuperación eficientes de datos.
- Evitar colisiones de claves mediante técnicas de resolución de colisiones, como el encadenamiento o la resolución lineal.

**Ejemplo:**

```go
package main

import (
    "fmt"
)

// Definición de una tabla hash como un mapa en Go
type HashTable map[string]int

// Función de dispersión (hash) simple para claves de cadena
func hashFunc(key string) int {
    hash := 0
    for _, char := range key {
        hash += int(char)
    }
    return hash
}

func main() {
    // Creación de una tabla hash
    hashTable := make(HashTable)
    
    // Añadir elementos a la tabla hash
    hashTable["apple"] = hashFunc("apple")
    hashTable["banana"] = hashFunc("banana")
    hashTable["orange"] = hashFunc("orange")
    
    // Imprimir la tabla hash
    fmt.Println("Hash Table:", hashTable)
    
    // Acceder a un elemento mediante su clave
    fmt.Println("Hash value of 'apple':", hashTable["apple"])
}
```

Volver al índice: [Recurso Go](README.md)
