# Errores en Go

En Go, el manejo de errores se realiza principalmente a través del uso de valores de error y la función error. No se utilizan excepciones como en algunos otros lenguajes de programación. En lugar de eso, Go sigue un enfoque de "manejo de errores explícito".

## Tipos de error

En Go, los errores son valores normales que se pueden asignar a variables y devolver desde funciones. El tipo de error en Go es simplemente error, que es una interfaz definida en la biblioteca estándar:

```go
type error interface {
    Error() string
}
```

Los tipos de error pueden ser cualquier tipo que implemente el método Error() string, que devuelve una cadena que describe el error.

## Excepciones

Go no tiene un sistema de excepciones como en otros lenguajes, donde se lanzan y capturan excepciones usando bloques try-catch. En su lugar, en Go, se espera que cada función que pueda fallar devuelva un valor de error, y el llamador maneja explícitamente este error.

```go
package main

import (
    "fmt"
)

// Función que retorna un error si el número es negativo
func dividir(a, b float64) (float64, error) {
    if b == 0 {
        return 0, fmt.Errorf("No se puede dividir por cero")
    }
        
    if a < 0 || b < 0 {
        return 0, fmt.Errorf("Los números negativos no son permitidos")
    }
        
    return a / b, nil
}

func main() {
    // Intentamos dividir dos números
    resultado, err := dividir(10, 0)
    if err != nil {
        fmt.Println("Error:", err)
        return
    }
        
    fmt.Println("Resultado:", resultado)
}

```

Volver al índice: [Recurso Go](README.md)