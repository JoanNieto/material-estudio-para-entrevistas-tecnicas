# Estructuras de datos funcionales en Go

## Pilas Persistentes (Persistent Stacks)

**Descripción:** Una pila persistente es una estructura de datos funcional que permite realizar operaciones de apilamiento (push) y desapilamiento (pop) de elementos, manteniendo versiones anteriores de la pila intactas. Esto significa que las operaciones de modificación no afectan a la versión anterior de la pila, lo que permite un acceso y manipulación seguros de datos históricos.

**Casos de Uso:**

- Historial de estados en aplicaciones donde se requiere reversión de acciones.
- Mantenimiento de registros de transacciones en sistemas de bases de datos.
- Implementación de editores de texto con capacidad de deshacer (undo).

**Ejemplo:**

```go
package main

import "fmt"

// Definición del nodo de la lista enlazada inmutable
type ListNode struct {
    value int
    next  *ListNode
}

// Función para crear un nuevo nodo de lista enlazada inmutable
func newNode(value int, next *ListNode) *ListNode {
    return &ListNode{value, next}
}

// Definición de la pila persistente
type PersistentStack struct {
    top *ListNode
}

// Función para apilar un elemento en la pila persistente
func (ps PersistentStack) push(value int) PersistentStack {
    return PersistentStack{newNode(value, ps.top)}
}

// Función para desapilar un elemento de la pila persistente
func (ps PersistentStack) pop() (int, PersistentStack) {
    if ps.top == nil {
        return 0, ps
    }
    return ps.top.value, PersistentStack{ps.top.next}
}

func main() {
    // Crear una pila persistente vacía
    stack := PersistentStack{}

    // Apilar elementos en la pila
    stack = stack.push(10)
    stack = stack.push(20)
    stack = stack.push(30)

    // Desapilar elementos de la pila
    fmt.Println("Pop:", stack.pop()) // Pop: 30
    fmt.Println("Pop:", stack.pop()) // Pop: 20
    fmt.Println("Pop:", stack.pop()) // Pop: 10
}
```

## Colas Persistentes (Persistent Queues)

**Descripción:** Una cola persistente es una estructura de datos funcional similar a una pila persistente, pero que permite realizar operaciones de inserción (enqueue) y eliminación (dequeue) de elementos, manteniendo versiones anteriores de la cola intactas. Esto permite un acceso y manipulación seguros de datos históricos, sin modificar las versiones anteriores de la cola.

**Casos de Uso:**

- Historial de estados en aplicaciones donde se requiere reversión de acciones.
- Mantenimiento de registros de transacciones en sistemas de bases de datos.
- Implementación de editores de texto con capacidad de deshacer (undo).

**Ejemplo:**

```go
package main

import "fmt"

// Definición de la cola persistente
type PersistentQueue struct {
    front *ListNode
    back  *ListNode
}

// Función para encolar un elemento en la cola persistente
func (pq PersistentQueue) enqueue(value int) PersistentQueue {
    newNode := newNode(value, nil)
    if pq.back != nil {
        pq.back.next = newNode
    }
    if pq.front == nil {
        pq.front = newNode
    }
    pq.back = newNode
    return pq
}

// Función para desencolar un elemento de la cola persistente
func (pq PersistentQueue) dequeue() (int, PersistentQueue) {
    if pq.front == nil {
        return 0, pq
    }
    value := pq.front.value
    front := pq.front.next
    if front == nil {
        pq.back = nil
    }
    return value, PersistentQueue{front, pq.back}
}

func main() {
    // Crear una cola persistente vacía
    queue := PersistentQueue{}

    // Encolar elementos en la cola
    queue = queue.enqueue(10)
    queue = queue.enqueue(20)
    queue = queue.enqueue(30)

    // Desencolar elementos de la cola
    fmt.Println("Dequeue:", queue.dequeue()) // Dequeue: 10
    fmt.Println("Dequeue:", queue.dequeue()) // Dequeue: 20
    fmt.Println("Dequeue:", queue.dequeue()) // Dequeue: 30
}

```

Volver al índice: [Recurso Go](README.md)
