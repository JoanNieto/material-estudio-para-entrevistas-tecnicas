# Funciones en Go

Las funciones son bloques de código reutilizables que realizan una tarea específica. Puedes definir tus propias funciones para encapsular la lógica y hacer que tu código sea más modular y fácil de mantener.
Recordemos que Go permite múliples retornos en las funciones

## Funciones Regulares

**Descripcion**: Las funciones regulares son bloques de código reutilizables que realizan una tarea específica.

**Uso**: Se utilizan para encapsular la lógica y hacer que el código sea más modular y fácil de mantener.

*Ejemplo:*

```go
package main

import "fmt"

// Definición de funcion
func suma(a, b int) int {
    return a + b
}

func main() {
    // Llamado a función
    resultado := suma(3, 5)
    fmt.Println("El resultado de la suma es:", resultado)
}

```

Devolviendo mas de un valor

```go
package main

import "fmt"

// Definición de funcion
func saludoError(nombre string) (string, error) {
    if nombre == "" {
        return "", fmt.Errorf("el nombre no puede estar vacío")
    }

    return "Hola, " + nombre, nil
}

func main() {
    // Llamado a función
    saludo, _ := saludoError("Mundo") // Guardamos solo el saludo, el error lo descartamos
    fmt.Println(saludo)
}

```

## Funciones Anónimas

**Descripcion**: Las funciones anónimas (o closures) son funciones que no tienen un nombre definido y pueden ser definidas en el lugar donde se necesiten.

**Uso**: Se utilizan para encapsular pequeñas piezas de lógica que no necesitan ser reutilizadas en otras partes del programa.

*Ejemplo:*

```go
package main

import "fmt"

func main() {
    // Función anónima
    suma := func(a, b int) int {
        return a + b
    }
    resultado := suma(3, 5)
    fmt.Println("El resultado de la suma es:", resultado)
}
```

## Funciones de orden superior

**Descripcion**: Las funciones de orden superior son aquellas que toman una o más funciones como argumentos y/o devuelven una función como resultado.

**Uso**: Se utilizan para escribir código más genérico y flexible.

*Ejemplo:*

```go
package main

import "fmt"

func aplicarFuncion(f func(int) int, numeros []int) []int {
    resultado := make([]int, len(numeros))
    for i, num := range numeros {
        resultado[i] = f(num)
    }
    
    return resultado
}

func doble(x int) int {
    return x * 2
}

func main() {
    numeros := []int{1, 2, 3, 4, 5}
    resultado := aplicarFuncion(doble, numeros)
    fmt.Println("Resultado:", resultado)
}
```

## Funciones recursivas

**Descripcion**: Las funciones recursivas son aquellas que se llaman a sí mismas dentro de su propio cuerpo.

**Uso**: Se utilizan para resolver problemas de manera iterativa mediante la repetición de una operación en subproblemas más pequeños.

*Ejemplo:*

```go
package main

import "fmt"

func factorial(n int) int {
    if n <= 1 {
        return 1
    }
    return n * factorial(n-1)
}

func main() {
    fmt.Println("Factorial de 5:", factorial(5))
}
```

## Funciones Variádicas

**Descripcion**: Descripción: Las funciones variádicas son aquellas que aceptan un número variable de argumentos.

**Uso**: Se utilizan cuando el número exacto de argumentos no es conocido de antemano.

*Ejemplo:*

```go
package main

import "fmt"

func sumaNumeros(numeros ...int) int {
    total := 0
    for _, num := range numeros {
        total += num
    }
    return total
}

func main() {
    fmt.Println("Suma de 1, 2 y 3:", sumaNumeros(1, 2, 3))
    fmt.Println("Suma de 4, 5, 6, 7 y 8:", sumaNumeros(4, 5, 6, 7, 8))
}
```

Volver al índice: [Recurso Go](README.md)