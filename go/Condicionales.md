# Condicionales en Go

En Go, al igual que en otros lenguajes de programación, puedes utilizar estructuras condicionales para controlar el flujo de tu programa. Aquí te muestro cómo funcionan:

## If (Si condicional)

Encontramos el uso de if o if/else cuya estructura básica es:

```go
if condición {
    // Código a ejecutar si la condición es verdadera
} else {
    // Código a ejecutar si la condición es falsa
}
```

### Uso sin else

```go
package main

import "fmt"

func main() {
    edad := 20

    if edad >= 18 {
        fmt.Println("Eres mayor de edad")
    }
}
```

### Uso con else y else if

```go
package main

import "fmt"

func main() {
    puntaje := 75

    if puntaje >= 90 {
        fmt.Println("Excelente")
    } else if puntaje >= 70 {
        fmt.Println("Buen trabajo")
    } else if puntaje >= 50 {
        fmt.Println("Puedes mejorar")
    } else {
        fmt.Println("Necesitas estudiar más")
    }
}
```

## Switch

Permite valuar múltiples condiciones y ejecutar diferentes bloques de código según el valor de una expresión, su estructura básica es:

```go
switch expresion {
case valor1:
    // Código a ejecutar si expresion == valor1
case valor2:
    // Código a ejecutar si expresion == valor2
...
default:
    // Código a ejecutar si ninguno de los casos anteriores coincide
}
```

### Uso general

```go
func main() {
    edad := 20

    switch {
    case edad < 18:
        fmt.Println("Eres menor de edad")
    case edad >= 18 && edad < 65:
        fmt.Println("Eres adulto")
    default:
        fmt.Println("Eres mayor de edad")
    }
}
```

### Uso de fallthrough

En Go, la ejecución de un case no se propaga automáticamente al siguiente caso. Sin embargo, puedes usar la palabra clave fallthrough para lograr este comportamiento:

```go
package main
import "fmt"

func main() {
    numero := 2

    switch numero {
    case 1:
        fmt.Println("Uno")
        fallthrough
    case 2:
        fmt.Println("Dos")
        fallthrough
    case 3:
        fmt.Println("Tres")
    }
}
```

En este caso la salida sera:

```go
Dos
Tres
```

Volver al índice: [Recurso Go](README.md)