# Estructuras de datos lineales en Go

## Arreglos

**Descripción:** Un arreglo es una colección de elementos del mismo tipo, con una longitud fija. Los elementos se almacenan en ubicaciones de memoria contiguas y se accede a ellos mediante índices.

**Casos de Uso:**

- Útil cuando se conoce de antemano el número de elementos y se necesita acceso rápido por índice.

**Ejemplo:**

```go
package main

import "fmt"

func main() {
    var arr [5]int // Declaración de un arreglo de 5 enteros
    arr[0] = 1
    arr[1] = 2
    arr[2] = 3
    arr[3] = 4
    arr[4] = 5

    for i := 0; i < len(arr); i++ {
        fmt.Println(arr[i])
    }
}
```

## Listas Enlazadas (Linked List)

**Descripción:** Una lista enlazada es una colección de nodos donde cada nodo contiene un valor y una referencia al siguiente nodo en la secuencia. No tienen tamaño fijo y pueden crecer o reducirse dinámicamente.

**Casos de Uso:**

- Implementaciones de pilas y colas
- Situaciones donde la inserción y eliminación de elementos son frecuentes
- Cuando se requiere una estructura de datos dinámica.

**Ejemplo:**

```go
package main

import "fmt"

// Nodo de la lista enlazada
type Node struct {
    data int
    next *Node
}

// Función para imprimir la lista enlazada
func printList(n *Node) {
    for n != nil {
        fmt.Print(n.data, " ")
        n = n.next
    }
    fmt.Println()
}

func main() {
    // Creación de nodos
    first := &Node{data: 10}
    second := &Node{data: 20}
    third := &Node{data: 30}
    
    // Enlazando los nodos
    first.next = second
    second.next = third
    
    // Imprimir la lista
    printList(first)
}
```

## Pilas (Stacks)

**Descripción:** Una pila es una estructura de datos LIFO (Last In, First Out) donde el último elemento en entrar es el primero en salir. Las operaciones principales son push (insertar) y pop (eliminar).

**Casos de Uso:**

- Algoritmos de retroceso (backtracking).
- Deshacer operaciones en editores.
- Evaluación de expresiones y análisis sintáctico..

**Ejemplo:**

```go
package main

import "fmt"

// Pila implementada usando slices
type Stack struct {
    items []int
}

// Operaciones de la pila
func (s *Stack) Push(item int) {
    s.items = append(s.items, item)
}

func (s *Stack) Pop() int {
    if len(s.items) == 0 {
        fmt.Println("Stack is empty")
        return -1
    }
    lastIndex := len(s.items) - 1
    item := s.items[lastIndex]
    s.items = s.items[:lastIndex]
    return item
}

func main() {
    stack := Stack{}
    stack.Push(10)
    stack.Push(20)
    stack.Push(30)
    
    fmt.Println(stack.Pop()) // 30
    fmt.Println(stack.Pop()) // 20
    fmt.Println(stack.Pop()) // 10
}

```

## Colas (Queues)

**Descripción:** Una cola es una estructura de datos FIFO (First In, First Out) donde el primer elemento en entrar es el primero en salir. Las operaciones principales son enqueue (insertar) y dequeue (eliminar).

**Casos de Uso:**

- Manejo de tareas en sistemas operativos.
- Implementación de algoritmos de búsqueda en grafos (BFS).
- Simulación de procesos en sistemas.

**Ejemplo:**

```go
package main

import "fmt"

// Cola implementada usando slices
type Queue struct {
    items []int
}

// Operaciones de la cola
func (q *Queue) Enqueue(item int) {
    q.items = append(q.items, item)
}

func (q *Queue) Dequeue() int {
    if len(q.items) == 0 {
        fmt.Println("Queue is empty")
        return -1
    }
    item := q.items[0]
    q.items = q.items[1:]
    return item
}

func main() {
    queue := Queue{}
    queue.Enqueue(10)
    queue.Enqueue(20)
    queue.Enqueue(30)
    
    fmt.Println(queue.Dequeue()) // 10
    fmt.Println(queue.Dequeue()) // 20
    fmt.Println(queue.Dequeue()) // 30
}
```

## Vectores (Vectors) / Slices

**Descripción:** Los vectores son similares a los arreglos, pero con la capacidad de cambiar de tamaño dinámicamente. En Go, los slices se usan para implementar vectores.

**Casos de Uso:**

- Manejo de tareas en sistemas operativos.
- Implementación de algoritmos de búsqueda en grafos (BFS).
- Simulación de procesos en sistemas.

**Ejemplo:**

```go
package main

import "fmt"

func main() {
    // Declaración y inicialización de un slice
    var vec []int = []int{10, 20, 30}
    
    // Añadir elementos al slice
    vec = append(vec, 40)
    vec = append(vec, 50)
    
    // Imprimir el slice
    fmt.Println(vec) // [10 20 30 40 50]
    
    // Acceso a elementos del slice
    fmt.Println(vec[0]) // 10
    fmt.Println(vec[1]) // 20
    
    // Modificación de un elemento
    vec[2] = 35
    fmt.Println(vec[2]) // 35
}
```

Volver al índice: [Recurso Go](README.md)
