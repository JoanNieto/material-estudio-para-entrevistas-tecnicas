# Guia de referencia para entrevistas técnicas: Lenguaje GO

Go es un lenguaje de programación estáticamente tipado, lo que significa que cada variable debe tener un tipo específico que se define en tiempo de compilación. En Go, los tipos de datos se dividen en varios grupos, incluyendo tipos básicos, tipos compuestos y tipos definidos por el usuario.

## Anotaciones del lenguaje

Es importante resaltar algunas características propias del lenguaje:

## Privacidad de acceso

En Go, el concepto de privacidad y visibilidad de las funciones y otros identificadores (variables, constantes, tipos, etc.) está determinado por convenciones y reglas relacionadas con la estructura del código y las reglas de acceso en el paquete.

## Multipes retornos

En Go, una función puede retornar múltiples valores de diferentes tipos de dato separados por comas.

## Contenido

1. [Tipos de datos](Tipos-de-datos.md)
2. [Operadores](Operadores.md)
3. [Variables y constantes](Variables-y-constantes.md)
4. [Condicionales](Condicionales.md)
5. [Bucles](Bucles.md)
6. [Funciones y llamadas](Funciones.md)
7. [Estructuras de datos](Estructura-de-datos.md)
   1. [Estructuras de datos lineales](Estructura-de-datos-lineales.md)
      1. [Arreglos (Arrays)](Estructura-de-datos-lineales.md#arreglos)
      2. [Listas enlazadas (Linked List)](Estructura-de-datos-lineales.md#listas-enlazadas-linked-list)
      3. [Pilas (Stacks)](Estructura-de-datos-lineales.md#pilas-stacks)
      4. [Colas (Queues)](Estructura-de-datos-lineales.md#colas-queues)
      5. [Vectores (Vectors)](Estructura-de-datos-lineales.md#vectores-vectors--slices)
   2. [Estructuras de datos jerárquicas](Estructura-de-datos-jerarquicos.md)
      1. [Árboles (Trees)](Estructura-de-datos-jerarquicos.md#arboles-trees)
      2. [Montículos (Heaps)](Estructura-de-datos-jerarquicos.md#montículos-heaps)
   3. [Estructuras de datos de grafos](Estructura-de-datos-grafos.md)
      1. [Grafos (graphs)](Estructura-de-datos-grafos.md#grafos-graphs)
   4. [Estructuras de datos de conjuntos](Estructura-de-datos-conjuntos.md)
      1. [Conjuntos (Sets)](Estructura-de-datos-conjuntos.md#conjuntos-sets)
      2. [Tablas Hash (Hash Tables)](Estructura-de-datos-conjuntos.md#tablas-hash-hash-tables)
   5. [Estructuras de datos especializados](Estructura-de-datos-especializados.md)
      1. [Tries (Prefijo árbol)](Estructura-de-datos-especializados.md#tries-árbol-de-prefijos)
      2. [Matrices dispersas (Sparse Matrices)](Estructura-de-datos-especializados.md#matrices-dispersas-sparse-matrices)
      3. [Listas doblemente terminadas (Doubly Ended Lists)](Estructura-de-datos-especializados.md#listas-doblemente-terminadas-doubly-ended-lists)
   6. [Estructuras de datos funcionales](Estructura-de-datos-funcionales.md)
      1. [Pilas persistentes (Persistent Stacks)](Estructura-de-datos-funcionales.md#pilas-persistentes-persistent-stacks)
      2. [Colas Persistentes (Persistent Queues)](Estructura-de-datos-funcionales.md#colas-persistentes-persistent-queues)
   7. [Estructuras de Concurrencia y paralelismo](Estructura-de-datos-concurrencia.md)
      1. [Colas Concurrentes (Concurrent Queues)](Estructura-de-datos-concurrencia.md#colas-concurrentes-concurrent-queues)
8. [Análisis de complejidad - Big-O Notation](Analisis-complejidad.md)
9. [POO](Poo.md)
   1. [Encapsulamiento, polimorfismo, herencia y abstracción](Poo.md#encapsulamiento-polimorfismo-herencia-y-abstracción)
   2. [Bajo acoplamiento y fuerte cohesión](Poo.md#bajo-acoplamiento-y-fuerte-cohesión)
   3. [⁠Interfaces y clases abstractas](Poo.md#interfaces-y-clases-abstractas)
   4. [⁠Inner class y clase anónima](Poo.md#inner-classes-y-clases-anónimas)
10. [Manejo de  errores](Errores.md)
    1. [Tipos de error](Errores.md#tipos-de-error)
    2. [Excepciones](Errores.md#excepciones)
11. [Clean Code](Clean-code.md)
    1. [DRY (Don't Repeat Yourself)](Clean-code-dry.md)
    2. [KISS (Keep It Simple, Stupid)](Clean-code-kiss.md)
    3. [SOLID](Clean-code-solid.md)
    4. Convenciones de nomenclatura
    5. Arte de documentar
    6. Linters y formateo
12. Bases de datos
    1. Tipos (SQL y NoSQL)
    2. Conceptos de normalización
    3. ACID
    4. Teorema CAP
    5. Replicación (sincrónica y asincrónica)
    6. Bases de datos en memoria
13. Patrones de diseño
    1. ¿Qué es?
    2. Tipos
    3. ¿Cuales he usado, cuales conozco?
14. Arquitectura de software
    1. Arquitectura en capas (Layered Architecture)
    2. Arquitectura cliente-servidor (Client-Server Architecture)
    3. Arquitectura basada en servicios (Service-Oriented Architecture, SOA)
    4. Arquitectura de microservicios (Microservices Architecture)
    5. Arquitectura orientada a eventos (Event-Driven Architecture, EDA)
    6. Arquitectura dirigida por dominios (Domain-Driven Design, DDD)
    7. Arquitectura hexagonal (Hexagonal Architecture)
    8. Arquitectura limpia (Clean Architecture)
    9. Arquitectura de tipo CQRS (Command Query Responsibility Segregation)
    10. Microservicios
15. Servicios web
    1. SOAP
    2. RESTful
    3. GrahpQL
    4. gRPC
    5. WebSocket
    6. WebHook
16. Seguridad
    1. Session
    2. JWT
    3. SSO
    4. OAuth
17. Contenedores y orquestación
    1. Docker
    2. Kubernetes
18. Despliegue y CI/CD
    1. Que es CI/CD
    2. Estrategias de despliegue (Canary, blue/green, rolling, etc)
    3. Herramientas: Jenkins, Gitlab CI/CD, Circle CI
19. Calidad de código
    1. Análisis estático de código
    2. Sonarqube
    3. OWASP
20. Pruebas de software
    1. Pruebas unitarias
    2. Pruebas de integración
    3. Pruebas funcionales
    4. Frameworks de pruebas
21. Trazas y métricas
    1. Logs
    2. Métricas
    3. Monitores
22. Colas de mensajes
    1. Kafka
    2. RabbitMQ
    3. SQS/SNS AWS
    4. Pub/Sub GoogleCloud
    5. Queue Storage/Service Bus Azure
23. Git
    1. Operaciones
    2. Gitflow
24. Frameworks y características específicas de lenguaje
