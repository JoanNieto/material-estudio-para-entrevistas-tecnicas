# Estructuras de datos en Go

1. [Estructuras de datos lineales](Estructura-de-datos-lineales.md)
2. [Estructuras de datos jerárquicas](Estructura-de-datos-jerarquicos.md)
3. [Estructuras de datos de grafos](Estructura-de-datos-grafos.md)
4. [Estructuras de datos de conjuntos](Estructura-de-datos-conjuntos.md)
5. [Estructuras de datos especializados](Estructura-de-datos-especializados.md)
6. [Estructuras de datos funcionales](Estructura-de-datos-funcionales.md)
7. [Concurrencia y paralelismo](Estructura-de-datos-concurrencia.md)

Volver al índice: [Recurso Go](README.md)
