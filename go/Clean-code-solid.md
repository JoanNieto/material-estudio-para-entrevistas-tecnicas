# SOLID en Go

SOLID es un acrónimo introducido por Robert C. Martin en su libro "Agile Software Development, Principles, Patterns and Practices" y hace referencia a los siguientes cinco principios:

- S: (SRP - Single responsibility principle) Principio de responsabilidad única.

- O: (OCP - Open closed principle) Principio abierto cerrado.

- L: (LSP - Liskov substitution principle) Principio de substitución de Liskov.

- I: (ISP - Interface segregation principle) Principio de segregación de la interfaz.

- D: (DIP - Dependency inversion principle) Principio de inversión de la dependencia.

El objetivo de aplicar estos principios es obtener sistemas orientados a objetos con código de mayor calidad, facilidad de mantenimiento y mejores oportunidades de reuso de código.

## S: (SRP - Single responsibility principle) Principio de responsabilidad única

> Una clase debe tener una, y sólo una, razón para cambiar, lo que significa que una clase debe tener un solo trabajo.

La primera observación respecto de este principio es que en Go no existen clases. Sin embargo, como vimos mediante la incorporación de comportamientos a tipos de datos, podemos llegar a un concepto equivalente.

Este principio hace foco en que un objeto debe tener únicamente una responsabilidad encapsulada por la clase. Cuando se hace referencia a una responsabilidad es para referirse a una razón para cambiar.
Mantener una clase que tiene múltiples objetivos o responsabilidades es mucho más complejo que una clase enfocada en una única responsabilidad.

```go
package main

import "fmt"

type Tarjeta struct {
    ID            string
    NumeroTarjeta int
    Tipo          string
    CCV           string
}

func (t *Tarjeta) CrearTarjeta(tipo string) {
    t.NumeroTarjeta = 123456789
    t.Tipo = tipo
    t.CCV = "123"
}

type Impresora struct {}

func (i *Impresora) ImprimirTarjeta(t Tarjeta) {
    fmt.Printf("Tarjeta: %v", t)
}

func main() {
    tarjeta := Tarjeta{}
    tarjeta.CrearTarjeta("debito")

    impresora := Impresora{}
    impresora.ImprimirTarjeta(tarjeta)
}
```

## O: (OCP - Open closed principle) Principio abierto cerrado

>Los objetos o entidades deberían estar abiertos para la extensión, pero cerrados para su modificación.

Este principio propone que una entidad esté cerrada, lista para ser usada y estable en su calidad e interfaces, y al mismo tiempo abierta, es decir, que permita extender su comportamiento (pero sin modificar su código fuente).

Siguiendo el ejemplo suponga que no solo va a imprimir por consola, sino a escribir la información de la tarjeta en un archivo.

```go
package main

import "fmt"

type Tarjeta struct {
    ID            string
    NumeroTarjeta int
    Tipo          string
    CCV           string
}

type TarjetaPrinter interface {
    Imprimir(t Tarjeta)
}

func main() {
    tarjeta := Tarjeta{}
    tarjeta.CrearTarjeta("debito")
    
    impresoraConsola := ConsolaPrinter{}
    impresoraConsola.Imprimir(tarjeta)
    
    impresoraArchivo := ArchivoPrinter{}
    impresoraArchivo.Imprimir(tarjeta)
}

func (t *Tarjeta) CrearTarjeta(tipo string) {
    t.NumeroTarjeta = 123456789
    t.Tipo = tipo
    t.CCV = "123"
}

type ConsolaPrinter struct{}

func (cp ConsolaPrinter) Imprimir(t Tarjeta) {
    fmt.Printf("Tarjeta: %v\n", t)
}

type ArchivoPrinter struct{}

func (ap ArchivoPrinter) Imprimir(t Tarjeta) {
    // Supongamos que aquí se escribe la tarjeta en un archivo
    fmt.Println("Tarjeta guardada en archivo.")
}

```

## L: (LSP - Liskov substitution principle) Principio de substitución de Liskov

> Cada clase que herede de otra debe poder utilizarse como su clase padre sin necesidad de conocer las diferencias que pudieran existir entre ellas.

Cada clase que herede de otra debe poder utilizarse como su clase padre sin necesidad de conocer las diferencias que pudieran existir entre ellas.

Para cumplir con el Principio de Sustitución de Liskov (LSP), las subclases deben poder ser sustituidas por sus clases base sin alterar el comportamiento correcto del programa. En nuestro ejemplo, podríamos tener diferentes tipos de tarjetas que hereden de la clase base Tarjeta, pero que aún así se comporten de manera coherente cuando se utilicen en lugar de la clase base.

```go
package main

import "fmt"

type Tarjeta interface {
    CrearTarjeta()
    ImprimirTarjeta()
}

type TarjetaBase struct {
    ID            string
    NumeroTarjeta int
    Tipo          string
    CCV           string
}

func (t *TarjetaBase) CrearTarjeta() {
    t.NumeroTarjeta = 123456789
    t.CCV = "123"
}

func (t *TarjetaBase) ImprimirTarjeta() {
    fmt.Printf("Tarjeta: %v\n", t)
}

type TarjetaDebito struct {
    TarjetaBase
}

type TarjetaCredito struct {
    TarjetaBase
    LimiteCredito int
}

func main() {
    tarjetaDebito := TarjetaDebito{}
    tarjetaDebito.CrearTarjeta()
    tarjetaDebito.ImprimirTarjeta()
    
    tarjetaCredito := TarjetaCredito{}
    tarjetaCredito.CrearTarjeta()
    tarjetaCredito.LimiteCredito = 1000
    tarjetaCredito.ImprimirTarjeta()
}

```

## I: (ISP - Interface segregation principle) Principio de segregación de la interfaz

>Nunca se debe obligar a un cliente a implementar una interfaz que no utilice, o no se debe forzar a los clientes a depender de métodos que no usan.

Este principio hace foco en como deben definirse las interfaces. Las mismas deben ser pequeñas y específicas.
Grandes y complejas interfaces obligan al cliente a implementar métodos que no necesita.

En nuestro ejemplo, podríamos aplicar el ISP asegurándonos de que las interfaces que definimos sean cohesivas y específicas para los clientes que las utilicen, por ejemplo imagine que solo la tarjeta de crédito tiene un calculo de su límite de dinero.

```go
package main

import "fmt"

// Interface para la creación de tarjetas
type TarjetaCreacion interface {
    CrearTarjeta()
}

// Interface para la impresión de tarjetas
type TarjetaImpresion interface {
    ImprimirTarjeta()
}

// Interface para calcular el límite que solo debe estar disponible en la tarjeta de crédito
type TarjetaCalculoLimite interface {
    CalcularLimite()
}

// Clase base para todas las tarjetas
type TarjetaBase struct {
    ID            string
    NumeroTarjeta int
    Tipo          string
    CCV           string
}

// Implementación de la creación de tarjetas para la tarjeta base
func (t *TarjetaBase) CrearTarjeta() {
    t.NumeroTarjeta = 123456789
    t.CCV = "123"
}

// Implementación de la impresión de tarjetas para la tarjeta base
func (t *TarjetaBase) ImprimirTarjeta() {
    fmt.Printf("Tarjeta: %v\n", t)
}

// Tarjeta de débito
type TarjetaDebito struct {
    TarjetaBase
}

// Implementación de la creación de tarjetas para la tarjeta de débito
func (t *TarjetaDebito) CrearTarjeta() {
    t.TarjetaBase.CrearTarjeta()
    // Posibles características específicas de la tarjeta de débito
}

// Tarjeta de crédito
type TarjetaCredito struct {
    TarjetaBase
    LimiteCredito int
}

// Implementación de la creación de tarjetas para la tarjeta de crédito
func (t *TarjetaCredito) CrearTarjeta() {
    t.TarjetaBase.CrearTarjeta()
    // Posibles características específicas de la tarjeta de crédito
}

func (t *TarjetaCredito) CalcularLimite() {
    t.LimiteCredito = 1000
}


func main() {
    tarjetaDebito := &TarjetaDebito{}
    tarjetaDebito.CrearTarjeta()
    tarjetaDebito.ImprimirTarjeta()

    tarjetaCredito := &TarjetaCredito{}
    tarjetaCredito.CrearTarjeta()
    tarjetaCredito.CalcularLimite()
    tarjetaCredito.ImprimirTarjeta()
}
```

## D: (DIP - Dependency inversion principle) Principio de inversión de la dependencia

> Los módulos de alto nivel no deben depender de módulos de bajo nivel. Ambos deberían depender de abstracciones.
>
> Las abstracciones no deben depender de los detalles. Los detalles deben depender de las abstracciones.

Este principio esta basado en reducir las dependencias entre los módulos del código para atacar el alto acoplamiento.

Además, las abstracciones no deben depender de detalles, sino que los detalles deben depender de las abstracciones. En términos prácticos, esto significa que los componentes de un sistema deben depender de interfaces o abstracciones en lugar de implementaciones concretas.

En el contexto del ejemplo, podríamos aplicar el DIP introduciendo interfaces para las dependencias externas, como la impresión de la tarjeta. De esta manera, los módulos de alto nivel (las clases TarjetaDebito y TarjetaCredito) dependerán de estas interfaces en lugar de depender directamente de las implementaciones concretas de impresión.

```go
package main

import "fmt"

// Interface para la creación de tarjetas
type TarjetaCreacion interface {
    CrearTarjeta()
}

// Interface para la impresión de tarjetas
type TarjetaImpresion interface {
    ImprimirTarjeta()
}

// Interface para calcular el límite de crédito
type TarjetaCreditoCalculo interface {
    CalcularLimite()
}

// Clase base para todas las tarjetas
type TarjetaBase struct {
    ID            string
    NumeroTarjeta int
    Tipo          string
    CCV           string
}

// Implementación de la creación de tarjetas para la tarjeta base
func (t *TarjetaBase) CrearTarjeta() {
    t.NumeroTarjeta = 123456789
    t.CCV = "123"
}

// Implementación de la impresión de tarjetas para la tarjeta base
func (t *TarjetaBase) ImprimirTarjeta() {
    fmt.Printf("Tarjeta: %v\n", t)
}

// Tarjeta de débito
type TarjetaDebito struct {
    TarjetaBase
}

// Implementación de la creación de tarjetas para la tarjeta de débito
func (t *TarjetaDebito) CrearTarjeta() {
    t.TarjetaBase.CrearTarjeta()
    // Posibles características específicas de la tarjeta de débito
}

// Tarjeta de crédito
type TarjetaCredito struct {
    TarjetaBase
    LimiteCredito int
}

// Implementación de la creación de tarjetas para la tarjeta de crédito
func (t *TarjetaCredito) CrearTarjeta() {
    t.TarjetaBase.CrearTarjeta()
    t.CalcularLimite() // Calculamos el límite de crédito al crear la tarjeta
}

// Implementación del cálculo del límite de crédito para la tarjeta de crédito
func (t *TarjetaCredito) CalcularLimite() {
    t.LimiteCredito = 1000 // Supongamos que aquí se calcula el límite de crédito
}

func main() {
    tarjetaDebito := &TarjetaDebito{}
    tarjetaDebito.CrearTarjeta()
    tarjetaDebito.ImprimirTarjeta()

    tarjetaCredito := &TarjetaCredito{}
    tarjetaCredito.CrearTarjeta()
    tarjetaCredito.ImprimirTarjeta()
}

```

Volver al índice: [Recurso Go](README.md)