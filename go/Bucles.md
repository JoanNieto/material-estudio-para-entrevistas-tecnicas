# Bucles en Go

Aunque existen diferentes tipos de bucles para repetir la ejecución de un bloque de código mientras se cumpla una condición o hasta que se alcance un determinado valor. Los tres tipos principales de bucles son for, while y do-while, sin embargo en Go solo se puede usar una forma de bucle, que es el bucle for.

```go
for inicialización; condición; post-ejecución {
    // Código a ejecutar en cada iteración
}
```

## Sintáxis básica

```go
package main

import "fmt"

func main() {
    for i := 1; i <= 5; i++ {
        fmt.Println(i)
    }
}
```

## Similar a while

```go
package main

import "fmt"

func main() {
    i := 1
    for i <= 5 {
        fmt.Println(i)
        i++
    }
}
```

## Bucle infinito

```go
package main

import "fmt"

func main() {
    for {
        fmt.Println("Este es un bucle infinito")
    }
}
```

Volver al índice: [Recurso Go](README.md)