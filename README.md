# Guia de referencia para entrevistas técnicas

## Motivación

Muchas veces nos encontramos con un desafío de que estudiar al momento de afrontar una entrevista técnica, el siguiente repositorio te ofrece una guía por donde empezar con diversos temas a tratar y sus implementaciones en algunos lenguajes.

## ¿Puedo contribuir?

Por supuesto, es una guía que debería estar en constante crecimiento (al mismo nivel que crece y evoluciona la industria tech). Siéntase libre de realizar un fork y agregar información y/o implementaciones en otros lenguajes.

## Contenido

1. Tipos de datos
2. Operadores
3. Variables y constantes
4. Condicionales
5. Bucles
6. Funciones y llamadas
7. Estructuras de datos
   1. Estructuras de datos lineales
      1. Arreglos (Arrays)
      2. Listas enlazadas (Linked List)
      3. Pilas (Stacks)
      4. Colas (Queues)
      5. Vectores (Vectors)
   2. Estructuras de datos jerárquicas
      1. Árboles (Trees)
      2. Montículos (Heaps)
   3. Estructuras de datos de grafos
      1. Grafos (graphs)
   4. Estructuras de datos de conjuntos
      1. Conjuntos (Sets)
      2. Tablas Hash (Hash Tables)
   5. Estructuras de datos especializados
      1. Tries (Prefijo árbol)
      2. Matrices dispersas (Sparse Matrices)
      3. Listas doblemente terminadas (Doubly Ended Lists)
   6. Estructuras de datos funcionales
      1. Pilas persistentes (Persistent Stacks)
      2. Colas Persistentes (Persistent Queues)
   7. Concurrencia y paralelismo
      1. Colas Concurrentes (Concurrent Queues)
8. Análisis de complejidad
   1. Big-O Notation
9. POO
   1. Encapsulamiento, polimorfismo, herencia y abstracción
   2. ⁠Bajo acoplamiento y fuerte cohesión
   3. ⁠Interfaces y clases abstractas
   4. ⁠Inner class y clase anónima
10. Bases de datos
    1. Tipos (SQL y NoSQL)
    2. Conceptos de normalización
    3. ACID
    4. Teorema CAP
    5. Replicación (sincrónica y asincrónica)
    6. Bases de datos en memoria
11. Clean Code
    1. Mantenibilidad, legibilidad, escalabilidad.
    2. DRY (Don't Repeat Yourself)
    3. KISS (Keep It Simple, Stupid)
    4. SOLID
    5. Principio de única responsabilidad (SRP)
    6. Convenciones de nomenclatura
    7. Arte de documentar
    8. Linters y formateo
12. Manejo de  errores
    1. Tipos de error
    2. Excepciones
13. Patrones de diseño
    1. ¿Qué es?
    2. Tipos
    3. ¿Cuales he usado, cuales conozco?
14. Arquitectura de software
    1. Arquitectura en capas (Layered Architecture)
    2. Arquitectura cliente-servidor (Client-Server Architecture)
    3. Arquitectura basada en servicios (Service-Oriented Architecture, SOA)
    4. Arquitectura de microservicios (Microservices Architecture)
    5. Arquitectura orientada a eventos (Event-Driven Architecture, EDA)
    6. Arquitectura dirigida por dominios (Domain-Driven Design, DDD)
    7. Arquitectura hexagonal (Hexagonal Architecture)
    8. Arquitectura limpia (Clean Architecture)
    9. Arquitectura de tipo CQRS (Command Query Responsibility Segregation)
    10. Microservicios
15. Servicios web
    1. SOAP
    2. RESTful
    3. GrahpQL
    4. gRPC
    5. WebSocket
    6. WebHook
16. Seguridad
    1. Session
    2. JWT
    3. SSO
    4. OAuth
17. Contenedores y orquestación
    1. Docker
    2. Kubernetes
18. Despliegue y CI/CD
    1. Que es CI/CD
    2. Estrategias de despliegue (Canary, blue/green, rolling, etc)
    3. Herramientas: Jenkins, Gitlab CI/CD, Circle CI
19. Calidad de código
    1. Análisis estático de código
    2. Sonarqube
    3. OWASP
20. Pruebas de software
    1. Pruebas unitarias
    2. Pruebas de integración
    3. Pruebas funcionales
    4. Frameworks de pruebas
21. Trazas y métricas
    1. Logs
    2. Métricas
    3. Monitores
22. Colas de mensajes
    1. Kafka
    2. RabbitMQ
    3. SQS/SNS AWS
    4. Pub/Sub GoogleCloud
    5. Queue Storage/Service Bus Azure
23. Git
    1. Operaciones
    2. Gitflow
24. Frameworks y características específicas de lenguaje

## Guias específicas

Verás la implementación de esta guía en su respectivo apartado de lenguaje:

1. [Recurso Go](go/README.md)
2. [Recurso Java](spring/README.md)
